package se.plankter.sscaitw;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Controller {

    @FXML
    private Button btnWatch;
    @FXML
    private TextField txtfAddWatch;
    @FXML
    private ListView<String> lstWatchList;
    @FXML
    private TextArea txtareaWebInformation;
    @FXML
    private MenuItem cntxtbtnRemoveItem;

    private String webInfo = "no info yet";
    private String folderName = "\\.sscaitw\\";
    private String fileName = "watchlist.txt";

    private Matchup nextMatchup;
    private NotificationHandler notificationHandler;

    public Controller() {
        notificationHandler = new NotificationHandler();
    }

    @FXML
    private void initialize() {
        loadFromFile();
        nextMatchup = new Matchup();


        Runnable webInfoRunnable = new Runnable() {
            @Override
            public void run() {
                fetchWebInformation();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        updateWebInformation();
                    }
                });

            }
        };
        int updateInterval = 5;
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(webInfoRunnable, 1, updateInterval, TimeUnit.SECONDS);

        updateContextMenu();
    }

    private void loadFromFile() {
        String folderPath = System.getProperty("user.home") + folderName;
        String filePath = folderPath + fileName;
        File folder = new File(folderPath);
        File file = new File(filePath);

        try {
            if (file.exists()) {
                List<String> names = Files.readAllLines(Paths.get(filePath));
                ObservableList<String> watchedNames = lstWatchList.getItems();
                for (String n : names) {
                    watchedNames.add(n);
                }
            }
            else {
                folder.mkdirs();
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToFile() {
        String folderPath = System.getProperty("user.home") + folderName;
        String filePath = folderPath + fileName;
        File folder = new File(folderPath);
        File file = new File(filePath);

        String content = watchlistToString();
        try {
            if (!file.exists()) {
                System.out.println("Path: " + filePath);

                folder.mkdirs();
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
            System.out.println("Saved to file");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String watchlistToString() {
        StringBuilder text = new StringBuilder();
        for (String s : lstWatchList.getItems()) {
            text.append(s + "\n");
        }
        return text.toString();
    }

    @FXML
    private void handleAddWatch() {
        if (txtfAddWatch.getText().length() < 1) {
            return;
        }
        ObservableList<String> watchItems = lstWatchList.getItems();
        String newItem = txtfAddWatch.getText();
        watchItems.add(newItem);
        txtfAddWatch.clear();
        updateContextMenu();
        saveToFile();
    }

    @FXML
    private void handleRemoveFromWatchlist() {
        // remove selected item
        ObservableList<String> watchItems = lstWatchList.getItems();
        ObservableList<String> selectedItems = lstWatchList.getSelectionModel().getSelectedItems();
        watchItems.removeAll(selectedItems);

        updateContextMenu();
        saveToFile();
    }

    private void updateContextMenu() {
        if (lstWatchList.getItems().size() > 0) {
            cntxtbtnRemoveItem.setDisable(false);
        }
        else {
            cntxtbtnRemoveItem.setDisable(true);
        }
    }

    private void updateWebInformation() {
        txtareaWebInformation.setText(webInfo);
    }

    private void fetchWebInformation() {
        Document doc;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        StringBuilder text = new StringBuilder("Information updated at: " + dateFormat.format(date) + "\n\n");
        try {
            doc = Jsoup.connect("http://sscaitournament.com/index.php?action=scores").get();
        } catch (Exception e) {
            System.out.println("Caught exception");
            e.printStackTrace();
            text.append(e.toString());
            webInfo = text.toString();
            return;
        }
        Element content = doc.getElementById("contentWrapper");
        Elements elements = content.getElementsContainingOwnText("vs");
        Element tenUpcoming = elements.first();
        String strNextTenMatches = tenUpcoming.ownText();
        String[] matchups =  strNextTenMatches.split(", ");

        String notifyMatch = null;
        int tmpNr = -1;
        text.append("Upcoming matches:\n");
        List<Object[]> comingWatched = new ArrayList<Object[]>();
        for (int i = 0; i < matchups.length; i++) {
            text.append(i+1 + ".\t" + matchups[i] + "\n");
            for (String watchName : lstWatchList.getItems()) {
                if (matchups[i].contains(watchName)) {
                    comingWatched.add(new Object[]{watchName, i});
                    if (notifyMatch == null) {
                        notifyMatch = matchups[i];
                        tmpNr = i;
                    }
                }
            }
        }


        text.append("\n");
        for (int i = 0; i < comingWatched.size(); i++) {
            String tmpString;
            int matchNr = (Integer)comingWatched.get(i)[1];
            if (matchNr == 0) {
                tmpString = " is playing right now!";
            } else if (matchNr == 1) {
                tmpString = " will play in the next match.";
            } else {
                tmpString = " will play in " + matchNr + " matches.";
            }
            text.append(comingWatched.get(i)[0] + tmpString + "\n");
        }


//        System.out.println("nextMatchup.getNumber(): " + nextMatchup.getNumber());
//        System.out.println("nextMatchup.getPlayers(): " + nextMatchup.getPlayers());
//        System.out.println("notifyMatch: " + notifyMatch);
//        System.out.println("tmpNr: " + tmpNr);
        if (notifyMatch != null && (nextMatchup.getNumber() != tmpNr || !notifyMatch.equalsIgnoreCase(nextMatchup.getPlayers()))) {
            nextMatchup.setPlayers(notifyMatch);
            nextMatchup.setNumber(tmpNr);
            notificationHandler.showNotification(nextMatchup);
        }

        webInfo = text.toString();
    }

}
