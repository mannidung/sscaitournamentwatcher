package se.plankter.sscaitw;

/**
 * Created by Plankton on 2014-12-25.
 */
public class Matchup {

    private String players;
    private int number;

    public Matchup() {
        players = null;
        number = -1;
    }

    public String getPlayers() {
        return players;
    }

    public int getNumber() {
        return number;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
